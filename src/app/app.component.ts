import { Component } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { Sensor } from './shared/models/sensor.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'my-project';
  sensors : Sensor[] = [];
  public i : number;

  constructor() {
    for (this.i = 0; this.i < 9; this.i++) {
      this.sensors.push(new Sensor(this.i, 'Датчик '+this.i));
    }
  }
  delete(index) {
    this.sensors.splice(index, 1);
  }

  add(name, check) {
    this.i++;
    this.sensors.push(new Sensor(this.i, name.value, check.checked));
  }
}
